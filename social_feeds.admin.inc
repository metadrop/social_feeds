<?php

/**
 * @file
 * Social Feeds module admin inc file.
 *
 * Contains admin configuration page callback functions.
 */

/**
 * Form builder; Configure social feeds settings for this site.
 *
 * @ingroup forms
 *
 * @see system_settings_form()
 */
function social_feeds_settings($form, &$form_state) {

  $form['social_feeds_date_format'] = [
    '#type' => 'textfield',
    '#title' => t('Data format'),
    '#default_value' => variable_get('social_feeds_date_format', 'd-m-Y H:i'),
    '#size' => 60,
    '#description' => t('Service data output format'),
    // '#required' => TRUE,.
  ];

  $form['facebook'] = [
    '#type' => 'fieldset',
    '#title' => t('Facebook'),
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $form['facebook']['social_feeds_facebook_app_id'] = [
    '#type' => 'textfield',
    '#title' => t('App Id'),
    '#default_value' => variable_get('social_feeds_facebook_app_id'),
    '#size' => 60,
    // @code '#required' => TRUE,
  ];

  $form['facebook']['social_feeds_facebook_app_secret'] = [
    '#type' => 'textfield',
    '#title' => t('App secret'),
    '#default_value' => variable_get('social_feeds_facebook_app_secret'),
    '#size' => 60,
    // @code '#required' => TRUE,
  ];

  $form['facebook']['social_feeds_facebook_access_token'] = [
    '#type' => 'textarea',
    '#title' => t('Access token'),
    '#default_value' => variable_get('social_feeds_facebook_access_token'),
    // @code '#required' => TRUE,
  ];

  $form['facebook']['social_feeds_facebook_graph_api_petition_type'] = [
    '#type' => 'select',
    '#title' => t('Graph api petition type'),
    '#default_value' => variable_get('social_feeds_facebook_graph_api_petition_type'),
    '#description' => l(t('Graph api info'), 'https://developers.facebook.com/docs/graph-api'),
    '#options' => [
      'GET' => t('GET'),
      'POST' => t('POST'),
      'DELETE' => t('DELETE'),
    ],
    // @code '#required' => TRUE,
  ];

  $form['facebook']['social_feeds_facebook_graph_api_petition'] = [
    '#type' => 'textfield',
    '#title' => t('Graph api petition'),
    '#default_value' => variable_get('social_feeds_facebook_graph_api_petition'),
    '#description' => l(t('Graph api info'), 'https://developers.facebook.com/docs/graph-api'),
    '#size' => 60,
    // @code '#required' => TRUE,
  ];

  $form['twitter'] = [
    '#type' => 'fieldset',
    '#title' => t('Twitter'),
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $form['twitter']['social_feeds_twitter_oauth_access_token'] = [
    '#type' => 'textfield',
    '#title' => t('Oauth access token'),
    '#default_value' => variable_get('social_feeds_twitter_oauth_access_token'),
    '#size' => 60,
    // @code '#required' => TRUE,
  ];

  $form['twitter']['social_feeds_twitter_oauth_access_token_secret'] = [
    '#type' => 'textfield',
    '#title' => t('Oauth access token secret'),
    '#default_value' => variable_get('social_feeds_twitter_oauth_access_token_secret'),
    '#size' => 60,
    // @code '#required' => TRUE,
  ];

  $form['twitter']['social_feeds_twitter_consumer_key'] = [
    '#type' => 'textfield',
    '#title' => t('Consumer key'),
    '#default_value' => variable_get('social_feeds_twitter_consumer_key'),
    '#size' => 60,
    // @code '#required' => TRUE,
  ];

  $form['twitter']['social_feeds_twitter_consumer_key_secret'] = [
    '#type' => 'textfield',
    '#title' => t('Consumer key secret'),
    '#default_value' => variable_get('social_feeds_twitter_consumer_key_secret'),
    '#size' => 60,
    // @code '#required' => TRUE,
  ];

  $form['twitter']['social_feeds_twitter_url'] = [
    '#type' => 'textfield',
    '#title' => t('Url'),
    '#default_value' => variable_get('social_feeds_twitter_url'),
    '#size' => 60,
    // @code '#required' => TRUE,
  ];

  $form['twitter']['social_feeds_twitter_parameters'] = [
    '#type' => 'textfield',
    '#title' => t('Parameters'),
    '#default_value' => variable_get('social_feeds_twitter_parameters'),
    '#size' => 60,
    // @code '#required' => TRUE,
  ];

  return system_settings_form($form);
}
